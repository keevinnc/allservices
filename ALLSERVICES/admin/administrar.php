 <!--
 Pagina privada para el administrador de la web
 -->
<?php
session_start();
if(!isset($_SESSION['logeado']) || $_SESSION["rol"]!="1"){
header ("Location: ../home.php");


}else{

  require_once('../modelo/class.conexion.php');
  require_once('../modelo/class.consultas.php');

   $consultas = new consultas();
   $email=$_SESSION['email'];
  $datos_anuncios=$consultas->consultaAnuncios("todos");
$datos_valoraciones=$consultas->consultaPuntuaciones();
$datos_usuarios=$consultas->listaTodosUsuarios();

}

 ?>

<html>
  <head>
    <meta charset="utf-8">
    <title>AllServices - tus servicios disponibles las 24H</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


<!--Inicio datatables-->
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>

    <link type="text/css" href="../css/jquery.dataTables.min.css" rel="stylesheet"/>
<!-- fin datatables-->
<link rel="stylesheet" href="../css/bootstrap.min.css" >
<script src="../js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../css/style.css" >

  </head>
  <body>

    <script type="text/javascript">

function confirm_delete() {
  return confirm('Esta seguro que quiere borrar este Servicio?');
}

$(document).ready(function() {
  $('#tabla_usuarios').DataTable( {
      responsive: true
  } );
} );

$(document).ready(function() {
  $('#tabla_anuncios').DataTable( {
      responsive: true
  } );
} );

$(document).ready(function() {
  $('#tabla_valoraciones').DataTable( {
      responsive: true
  } );
} );


</script>
<!-- inicio header -->
<header>
  <div class="container">
      <h1>Administración</h1>
  </div>
</header>
<!-- fin header -->


<!-- inicio  go-home -->

    <nav class="navbar navbar-default navbar-inverse" role="navigation">

      <div class="container">

    			<div class="navbar-header">

    			  <a class="navbar-brand" href="../home.php">AllServices</a>
    			</div>
      </div>
    </nav>

<!-- fin  barra-go-home -->



<!-- inicio container -->
<div class="container col-xs-12  col-sm-12   col-md-12  col-lg-12   ">
<br>

<!-- Inicio PANEL -->


 <div class="container">

   <?php
   if(isset($_GET["borrar_servicio_ok"])){
     echo "<div id='usuario_creado' class='alert alert-success'>Servicio borrado correctamente</div> ";
   }
   if(isset($_GET["borrar_servicio_error"])){
     echo "<div id='usuario_creado' class='alert alert-danger'>Error al borrar el servicio</div> ";
   }
   if(isset($_GET["borrar_valoracion_ok"])){
     echo "<div id='usuario_creado' class='alert alert-success'>Valoración borrada correctamente</div> ";
   }
   if(isset($_GET["borrar_valoracion_error"])){
     echo "<div id='usuario_creado' class='alert alert-danger'>Error al borrar la valoración</div> ";
   }
   if(isset($_GET["borrar_usuario_ok"])){
     echo "<div id='usuario_creado' class='alert alert-success'>Usuario borrado correctamente</div> ";
   }
   if(isset($_GET["borrar_usuario_error"])){
     echo "<div id='usuario_creado' class='alert alert-danger'>Error al borrar el usuario</div> ";
   }

    ?>

   <h2>Bienenivo   <?php echo $_SESSION['nombre']  ?> <span class='glyphicon glyphicon-check'></span></h2><br>
   <ul class="nav nav-tabs">
     <li class="active"><a data-toggle="tab" href="#home">Usuarios <span class='glyphicon glyphicon-user'> </a></li>
     <li><a data-toggle="tab" href="#menu1">Anuncios <span class='glyphicon glyphicon-list'></a></li>
     <li><a data-toggle="tab" href="#menu2">Puntuaciones <span class='glyphicon glyphicon-thumbs-up'></a></li>
   </ul>

   <div class="tab-content">
     <div id="home" class="tab-pane fade in active">
       <h3>Usuarios <span class='glyphicon glyphicon-user'></h3>
         <a href="../php_excel/exportar_todo.php?tabla=usuarios"><h4 align="right" style="color:#43a75a" >Exportar a excel <span  class='glyphicon glyphicon-folder-open'></h4></a><br>
         <!-- Inicio PANEL -->

           <table id="tabla_usuarios"  class="table table-striped display"  cellspacing="0" width="100%">
              <thead>
                 <tr class="info text-center"><td><h4>id</h4></td><td><h4>Nombre</h4></td><td><h4>Email</h4></td><td><h4>Fecha creacion</h4></td><td><h4>Rol</h4></td><td><h4>Eliminar</h4></td></tr>
              </thead>
              <tbody>
                <?php
                foreach ($datos_usuarios as $rs) {
                      $id_usuario=$rs['id_usuario'];
                      $email=$rs['email'];
                      $nombre=$rs['nombre'];
                      $fecha_creacion=$rs['fecha_creacion'];
                      $roles_id_rol=$rs['roles_id_rol'];

                    echo " <tr class='text-center'><td>$id_usuario</td><td>$nombre</td><td>$email</td><td>$fecha_creacion</td><td>$roles_id_rol</td> <td>   <a href='../controlador/borrar_usuario.php?email_anunciante=$email' onclick='return confirm_delete()'><span class='glyphicon glyphicon-remove'></span> </a></td></tr>";
                }
                 ?>
              </tbody>
           </table>

         <!-- fin PANEL -->
     </div>
     <div id="menu1" class="tab-pane fade">
       <h3>Anuncios <span class='glyphicon glyphicon-list'></h3>
        <a href="../php_excel/exportar_todo.php?tabla=anuncios"><h4 align="right" style="color:#43a75a" >Exportar a excel <span  class='glyphicon glyphicon-folder-open'></h4></a><br>

           <table id="tabla_anuncios"  class="table table-striped display"  cellspacing="0" width="100%">
               <thead>
                  <tr class="info"><td><h4>Titulo</h4></td><td><h4>Descripcion</h4></td><td><h4>Email</h4></td><td><h4>Codigo postal</h4></td><td><h4>telefono</h4></td><td><h4>Categoria</h4></td><td><h4>Precio</h4></td><td><h4>Fecha creacion</h4></td><td><h4>Ver</h4></td><td><h4>Eliminar</h4></td></tr>
               </thead>
               <tbody>
                 <?php
                 foreach ($datos_anuncios as $rs) {
                       $id_anuncio=$rs['id_anuncio'];
                       $titulo=$rs['titulo'];
                       $descripcion=$rs['descripcion'];
                       $email=$rs['email'];
                       $codigo_postal=$rs['codigo_postal'];
                       $telefono=$rs['telefono'];
                       $categoria=$rs['categoria'];
                       $precio=$rs['precio'];
                       $fecha_creacion=$rs['fecha_creacion'];


                     echo " <tr><td>$titulo</td><td>$descripcion</td><td>$email</td><td>$codigo_postal</td><td>$telefono</td><td>$categoria</td><td>$precio</td><td>$fecha_creacion</td><td><a href='../anuncio.php?id_anuncio=$id_anuncio'><span class='glyphicon glyphicon-list-alt'></span> </a></td><td>   <a href='../controlador/borrar_anuncio.php?id_anuncio=$id_anuncio&email_anunciante=$email' onclick='return confirm_delete()'><span class='glyphicon glyphicon-remove'></span> </a></td></tr>";
                 }
                  ?>
               </tbody>
            </table>
     </div>
     <div id="menu2" class="tab-pane fade">
       <h3>Valoraciones <span class='glyphicon glyphicon-thumbs-up'></h3>


         <!-- Inicio PANEL -->


         <table id="tabla_valoraciones" class="table table-striped" >
              <thead>
                 <tr class="info text-center"><td><h4>id</h4></td><td><h4>Puntos</h4></td><td><h4>Comentario</h4></td><td><h4>Usuario</h4></td><td><h4>Anuncio</h4></td></tr>
              </thead>
              <tbody>
                <?php
                foreach ($datos_valoraciones as $rs) {
                      $id_valoracion=$rs['id_valoracion'];
                      $valoracion=$rs['valoracion'];
                      $comentario=$rs['comentario'];
                      $email_user_voto=$rs['email_user_voto'];
                      $id_user_voto=$rs['id_user_voto'];
                       $id_anuncio=$rs['anuncios_id_anuncio'];

                    echo " <tr class='text-center'><td>$id_valoracion</td><td>$valoracion</td><td>$comentario</td><td>$email_user_voto</td><td>   <a href='../controlador/borrar_valoracion.php?id_valoracion=$id_valoracion' onclick='return confirm_delete()'><span class='glyphicon glyphicon-remove'></span> </a></td></tr>";
                }
                 ?>
              </tbody>
           </table>

         <!-- fin PANEL -->
     </div>

   </div>
 </div>



<!-- fin PANEL -->


</div>
<!-- fin container -->



  </body>
</html>
