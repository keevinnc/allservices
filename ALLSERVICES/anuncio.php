<!--
 Pagina para subir anuncio
 -->
<?php



if(isset($_GET['id_anuncio'])){

  require_once('modelo/class.conexion.php');
  require_once('modelo/class.consultas.php');

  $id_anuncio=$_GET['id_anuncio'];
   $consultas = new consultas();
  $datos_anuncio=$consultas->consultaAnuncio($id_anuncio);
  foreach ($datos_anuncio as $rs) {
        $titulo=$rs['titulo'];
        $descripcion=$rs['descripcion'];
        $email=$rs['email'];
        $codigo_postal=$rs['codigo_postal'];
        $telefono=$rs['telefono'];
        $categoria=$rs['categoria'];
        $precio=$rs['precio'];
        $fecha_creacion=$rs['fecha_creacion'];
        $imagen_1=$rs['imagen_1'];

  }
}else {
  header ("Location: home.php");
  break;
}


 ?>

<html>
  <head>
    <meta charset="utf-8">
    <title>AllServices - tus servicios disponibles las 24H</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="js/jquery-1.12.3.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css" >
<!-- INICIO link para el mapa -->
      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
      <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
      <script type="text/javascript" src="js/gmaps.js"></script>
      <link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.3.0/bootstrap.min.css" />
      <link rel="stylesheet" type="text/css" href="css/style.css" />
      <script type="text/javascript">
        var map;
        $(document).ready(function(){

        map = new GMaps({

            el: '#map',
            lat: 0,
            lng:0,

          });

          GMaps.geocode({
            address:" <?php echo $codigo_postal ?>, Spain",
            callback: function(results, status){
              if(status=='OK'){
                var latlng = results[0].geometry.location;
                map.setCenter(latlng.lat(), latlng.lng());
                map.addMarker({
                  lat: latlng.lat(),
                  lng: latlng.lng()
                });
              }
            }
          });



        });
      </script>

<!--FIN link para el mapa -->



  </head>
  <body>
<!-- inicio header -->
<header>
  <div class="container">
      <h1>Anuncio</h1>
  </div>
</header>
<!-- fin header -->


<!-- inicio  go-home -->

    <nav class="navbar navbar-default navbar-inverse" role="navigation">

      <div class="container">

    			<div class="navbar-header">

    			  <a class="navbar-brand" href="home.php">AllServices</a>
    			</div>
      </div>
    </nav>

<!-- fin  barra-go-home -->



<!-- inicio container -->
<div class="container col-xs-12  col-sm-6 col-md-6  col-lg-6 col-sm-offset-6 col-md-offset-3 col-lg-offset-3  ">
<br>
<?php
//if(isset($_GET["anuncio_creacion_ok"])){
//  echo "<div id='usuario_creado' class='alert alert-success'>Anuncio creado correctamente, <a href='#'>compruebalo.</a></div> ";
//}
 ?>

<!-- Inicio PANEL -->

<div>
    <div class='panel panel-default'>
      <div class="panel-heading "><h3 align="left" style="color:#43a75a"><?php echo $titulo; ?></h3></div>
      <div class="panel-body text-center">  <img align="center" src="<?php echo $imagen_1; ?> "   width='300px' class = "img-rounded ">
        <br><br><br><br>
        <p align="text-center" > <?php echo $descripcion; ?> </p>
      <h3 align="right" style="color:#43a75a">Precio: <?php echo $precio; ?>€</h3><br><br>

      <!-- Inicio MAPA -->
      <div class="row">
        <div class="span11">
          <div id="map"></div><br/>
        </div>

      </div>
      <!-- Fin MAPA -->




      </div>
            <div class="panel-footer">
              <p><h4 style="color:#43a75a">Categoria:</h4><?php echo $categoria; ?></p>
                <p><h4 style="color:#43a75a"> Telefono de contacto:</h4> <?php echo $telefono; ?></p>
              <div class="text-center">
              <!--   <img class="center-block" src="images/icons/icon_login.png" alt="icon_image"  width="50px"class="img-rounded">
                <p><?php  /** echo $nombre_creador */ ?></p>-->

                <a href="email/formulario.php?id_anuncio=<?php echo $id_anuncio;?>&titulo=<?php echo $titulo;?>&email_anunciante=<?php echo $email;?>"><h3>Contactar con el anunciante</h3></a>
<p align="right">
  <a href="anuncio_valorar.php?id_anuncio=<?php echo $id_anuncio;?>&email_anunciante=<?php echo $email;?>">
    <button type="button" class="btn btn-success btn-sm">
      <span class="glyphicon glyphicon-thumbs-up"></span>Puntuar servicio</button>
</a>

</p>
    <p align="left" style="color:#43a75a"> Creado el <?php echo $fecha_creacion; ?></p>


              </div>
            </div>
    </div>

</div>

<!-- fin PANEL -->


</div>
<!-- fin container -->



  </body>
</html>
