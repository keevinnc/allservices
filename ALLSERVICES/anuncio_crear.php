<!--
 Pagina para subir anuncio
 -->
<?php
session_start();
if(!isset($_SESSION['logeado'])){
  header ("Location: usuario_login.php?error_publicar_anuncio");
}
 ?>

<html>
  <head>
    <meta charset="utf-8">
    <title>AllServices - tus servicios disponibles las 24H</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/jquery-1.12.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="css/style.css" >
      <link rel="stylesheet" href="css/bootstrap.min.css" >
  </head>
  <body>
<!-- inicio header -->
<header>
  <div class="container">
      <h1>Publicar anuncio</h1>
  </div>
</header>
<!-- fin header -->


<!-- inicio  go-home -->

    <nav class="navbar navbar-default navbar-inverse" role="navigation">

      <div class="container">

    			<div class="navbar-header">

    			  <a class="navbar-brand" href="home.php">AllServices</a>
    			</div>
      </div>
    </nav>

<!-- fin  barra-go-home -->



<!-- inicio container -->
<div class="container col-xs-12  col-sm-6 col-md-6  col-lg-5 col-sm-offset-3 col-md-offset-3 col-lg-offset-4  ">
<br>
<?php

if(isset($_GET["anuncio_creacion_ok"])){
  echo "<div id='usuario_creado' class='alert alert-success'>Anuncio creado correctamente.</div> ";
}if(isset($_GET["error_creacion"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Error al crear el anuncio, vuelva a intentarlo.</div> ";
}


 ?>


<!-- Inicio Formulario -->
<form class="" action="controlador/anuncioCrear.php" method="post" enctype="multipart/form-data">
  <div class="well well-sm">
  <div class="form-group">
    <label for="titulo">titulo:</label>
    <input class="form-control" id="titulo" type="text" name="titulo" placeholder="titulo" required>
  </div>
  <div class="form-group">
    <label for="codigo_postal">codigo postal:</label>
    <input class="form-control" id="codigo_postal" type="text" name="codigo_postal" placeholder="codigo_postal" required>
  </div>
  <div class="form-group">
    <label for="telefono">telefono contacto:</label>
    <input class="form-control" id="telefono" name="telefono" type="text" placeholder="telefono" required>
  </div>




<div class="form-group">
  <label class="control-label" for="option">Categoria:</label>

  <?php
  require_once('modelo/class.conexion.php');
  $modelo = new Conexion(); /* creamos un objeto de conexion */
  $pdo = $modelo->get_conexion();  /* aquí guardamos la conexion */

  try {
    $sql = 'SELECT * FROM categorias ';
    $query = $pdo->prepare($sql);
    $query->execute();
    $list = $query->fetchAll();
  }catch (PDOException $e) {
  	echo 'PDOException : '.  $e->getMessage();
  }
  echo '<select class="form-control" name="categoria" id="option" required >';
      echo ' <option  selected="selected">Selecciona </option>';
  foreach ($list as $rs) {
      echo '<option value='.$rs['nombre_categoria'].'>'.$rs['nombre_categoria'].'</option>';
      //<h2>'.$rs['nombre_categoria'].'</h2>';
  }
    echo '</select>';
   ?>


</div>
<div class="form-group">
    <label for="precio">precio:</label>
    <input type="number" class="form-control" name="precio" min="1" max="1000" placeholder="precio/hora" required>
</div>
<div class="form-group">
    <label for="Horario">Horario disnibilidad:</label>
      <p class="help-block">inicio </p>
    <div class="input-group ">
        <input id="hora_inicial" type="time" name="hora_inicial" class="form-control input-small" placeholder="hora inicial">
        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
    </div>
    <p class="help-block">final </p>
    <div class="input-group ">
        <input id="hora_final" type="time"   name="hora_final" class="form-control input-small" placeholder="hora final">
        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
    </div>

</div>

<div class="form-group">
  <label for="descripcion">Descripcion del servicio:</label>
  <textarea class="form-control"  name="descripcion" rows="8" cols="40" placeholder="Breve explicación del servicio" required></textarea>

</div>

<div class="form-group">
  <label for="imagenes">Imagenes</label>
  <!-- <input   	 name="userfile"  type="file" id="imagen"   accept="image/x-png, image/jpeg" required>-->
     	<input name="userfile" type="file" accept="image/x-png, image/jpeg" required>
  <p class="help-block">Formato png o jpeg </p>
</div>
<button class="btn btn-success btn-lg btn-block" name="subir" type="submit" >Subir</button>
</div>
</form>
<!-- fin formulario -->
</div>
<!-- fin container -->






  </body>
</html>
