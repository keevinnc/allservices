<!--
 Pagina para subir anuncio
 -->
<?php
session_start();
if(!isset($_SESSION['logeado'])){
  header ("Location: usuario_login.php?error_valorar_anuncio");
    break;
}

if(isset($_GET['id_anuncio'])){
$id_anuncio=$_GET['id_anuncio'];
}
if(isset($_GET['email_anunciante'])){
$email_anunciante=$_GET['email_anunciante'];
}


 ?>

<html>
  <head>
    <meta charset="utf-8">
    <title>AllServices - tus servicios disponibles las 24H</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script  src="js/jquery.min.js"></script>
    <script src="js/jquery-1.12.3.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css" >
      <link rel="stylesheet" href="css/style.css" >
  </head>
  <body>
<!-- inicio header -->
<header>
  <div class="container">
      <h1>Valorar anuncio</h1>
  </div>
</header>
<!-- fin header -->


<!-- inicio  go-home -->

    <nav class="navbar navbar-default navbar-inverse" role="navigation">

      <div class="container">

    			<div class="navbar-header">

    			  <a class="navbar-brand" href="home.php">AllServices</a>
    			</div>
      </div>
    </nav>

<!-- fin  barra-go-home -->



<!-- inicio container -->
<div class="container col-xs-12  col-sm-6 col-md-6  col-lg-5 col-sm-offset-3 col-md-offset-3 col-lg-offset-4  ">
<br>
<?php

if(isset($_GET["user_valoracion_ok"])){
  echo "<div id='usuario_creado' class='alert alert-success'>Anuncio valorado correctamente.</a></div> ";
}
if(isset($_GET["user_valoracion_error"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Hay un problema, no se ha podido registrar la valoración.</a></div> ";
}


 ?>


<!-- Inicio Formulario -->
<form class="" action="controlador/anuncioValorar.php" method="post">
  <div class="well well-sm">

<div class="form-group" >
  <label class="control-label" for="option">Puntuación: <span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></label>
  <select class="form-control" name="puntuacion" id="puntuacion" required>
    <option  selected="selected">Selecciona </option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
  </select>
</div>


<div class="form-group">
  <label for="descripcion">Comentario:</label>
  <textarea class="form-control"  name="comentario" rows="4" cols="20" placeholder="Breve cometario relacionado con el servicio" required></textarea>
<input type="hidden"  id="id_anuncio" name="id_anuncio"  value="<?=$id_anuncio ?>" >
<input type="hidden"  id="email_anunciante" name="email_anunciante"  value="<?=$email_anunciante ?>" >


</div>


<button class="btn btn-success btn-lg btn-block" name="subir">Puntuar</button>
</div>
</form>
<!-- fin formulario -->
</div>
<!-- fin container -->






  </body>
</html>
