<?php
session_start();

  require_once("../modelo/class.conexion.php");
  require_once("../modelo/class.consultas.php");


  #recibimos los datos del form_crearAnuncio y
  #saneamos datos:

  //var_dump($_POST);


  $consultas = new consultas();
    $titulo=$consultas->test_input($_POST['titulo']);
    $codigo_postal=$consultas->test_input($_POST['codigo_postal']);
    $telefono=$consultas->test_input($_POST['telefono']);
    $descripcion=$consultas->test_input($_POST['descripcion']);
    #FILTRAMOS
    $titulo_saneado = filter_var($titulo,FILTER_SANITIZE_STRING);  #Elimina etiquetas, opcionalmente elimina o codifica caracteres especiales
    $codigo_postal_saneado = filter_var($codigo_postal,FILTER_SANITIZE_STRING);  #Elimina etiquetas, opcionalmente elimina o codifica caracteres especiales
    $telefono_saneado = filter_var($telefono,FILTER_SANITIZE_STRING);  #Elimina etiquetas, opcionalmente elimina o codifica caracteres especiales
    $descripcion_saneado = filter_var($descripcion,FILTER_SANITIZE_STRING);  #Elimina etiquetas, opcionalmente elimina o codifica caracteres especiales
    $categoria=$_POST['categoria'];
    $precio=$_POST['precio'];
    $hora_inicial=$_POST['hora_inicial'];
    $hora_final=$_POST['hora_final'];
    $imagen="images/img_anuncios/".$_FILES['userfile']['name'];
    $id_usuario=$_SESSION["id_usuario"];
    $email=$_SESSION["email"];
    //datos de la imagen







    #creamos las fechas
    $fecha_creacion=date("Y-m-d H:i:s");
    $fecha_caduca=date("Y-m-d H:i:s", strtotime("+1 month"));
  #insertamos datos
    $mensaje=null;
    $mensaje= $consultas->crearAnuncio($titulo_saneado, $descripcion_saneado, $email, $codigo_postal_saneado, $telefono_saneado, $categoria, $precio, $fecha_creacion, $fecha_caduca, $imagen, $id_usuario, 1);
    move_uploaded_file($_FILES['userfile']['tmp_name'], '../'.$imagen);
  #si se ha creado correctamente redirigimos con ok por GET, para mostrar mensaje


    if($mensaje=="ok"){

       header ("Location: ../anuncio_crear.php?anuncio_creacion_ok");
         break;
    }else{
        header ("Location: ../anuncio_crear.php?error_creacion");
         break;
    }




 ?>
