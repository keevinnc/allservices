<?php
session_start();

if(!isset($_SESSION['logeado'])){
  header ("Location: ../home.php");
}

$email_user=$_SESSION['email'];
$id_anuncio=$_GET['id_anuncio'];
$email_anunciante=$_GET['email_anunciante'];

#Comprobamos si el usuario es administrador:
if ($_SESSION['rol']=="1") {

  require_once("../modelo/class.conexion.php");
  require_once("../modelo/class.consultas.php");
  $consultas = new consultas();
  #borramos anuncio
    $consultas->borrarValoracion($email_anunciante,"email");
      $mensaje= $consultas->borrarAnuncio($id_anuncio,"id");
    #si se ha borrado correctamente redirigimos con ok por GET, para mostrar mensaje
    echo $mensaje;
      if($mensaje=="ok"){
          header ("Location: ../admin/administrar.php?borrar_servicio_ok");
           break;
      }else{
         header ("Location: ../admin/administrar.php?borrar_servicio_error");
           break;
      }

}

#Si no es administrador, pero quiere eliminar sus anuncios ejecutamos.
#Comprobamos que el usuario que solicita borrar el anuncio es el propietario del anuncio
if ($email_anunciante == $email_user ) {
  require_once("../modelo/class.conexion.php");
  require_once("../modelo/class.consultas.php");
  $consultas = new consultas();
  #borramos anuncio
    $consultas->borrarValoracion($email_anunciante,"email");
    $mensaje= $consultas->borrarAnuncio($id_anuncio);
  #si se ha borrado correctamente redirigimos con ok por GET, para mostrar mensaje
  echo $mensaje;
    if($mensaje=="ok"){
        header ("Location: ../tus_anuncios.php?borrar_ok");
         break;
    }else{
       header ("Location: ../tus_anuncios.php?borrar_error");
         break;
    }
}else{
  header ("Location: ../tus_anuncios.php?borrar_no_permitido");
  break;
}








 ?>
