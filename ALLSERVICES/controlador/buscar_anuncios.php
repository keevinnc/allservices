<?php



function buscar($busqueda,$categoria){

  $consultas = new Consultas();
  $busqueda_saneado=$consultas->test_input($busqueda);
  $busqueda_saneado = filter_var($busqueda_saneado,FILTER_SANITIZE_STRING);

  $filas = $consultas->buscarAnuncio($busqueda_saneado,$categoria);
  if (isset($filas)) {
?>
    <aside class='col-xs-12 col-sm-9 '>


      <?php
      $last_id = 0;
      foreach ($filas as $rs) {
          $last_id = $rs['id_anuncio']; // contiene el ultimo id de la pagina
          ?>
  <a href="anuncio.php?id_anuncio=<?php echo $rs['id_anuncio']; ?>">

            <div>
                <div class='panel panel-default'>
                  <div class="panel-heading"><h3 style="color:#43a75a"><?php echo $rs['titulo']; ?></h3></div>
                  <div class="panel-body">  <img src="<?php echo $rs['imagen_1']; ?> " alt=" <?php echo $rs['id_anuncio']; ?>"  width='300px'></div>
                    <div class="panel-body">
                      <h3 align="right" style="color:#43a75a">Precio: <?php echo $rs['precio']; ?>€</h3>

                        <div class="panel-footer"><p><?php echo $rs['categoria']; ?> <br>Creado el <?php echo $rs['fecha_creacion']; ?></p></div>

                    </div>
                </div>

            </div>
  </a>


          <?php
      }
      ?>
    </aside>


<?php

}else{
echo  "  <aside class='col-xs-12 col-sm-9 '>  <div id='usuario_creado' class='alert alert-danger'> <p>Parece que por el momento lo que buscas no se encuentra en Allservices.</p>
 </div></aside>";
}
}
 ?>
