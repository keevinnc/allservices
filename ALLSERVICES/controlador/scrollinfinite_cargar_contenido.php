<?php
// including the config file
require_once('../modelo/class.conexion.php');
$modelo = new Conexion(); /* creamos un objeto de conexion */
$pdo = $modelo->get_conexion();  /* aquí guardamos la conexion */


$last_id = $_POST['last_id'];
$limit = 5; // default value
if (isset($_POST['limit'])) {
	$limit = intval($_POST['limit']);
}


try {
	$sql = 'SELECT * FROM anuncios WHERE id_anuncio > :last_id ORDER BY id_anuncio ASC LIMIT 0, :limit';
	$query = $pdo->prepare($sql);
	$query->bindParam(':last_id', $last_id, PDO::PARAM_INT);
	$query->bindParam(':limit', $limit, PDO::PARAM_INT);
	$query->execute();
	$list = $query->fetchAll();
} catch (PDOException $e) {
	echo 'PDOException : '.  $e->getMessage();
}

$last_id = 0;
foreach ($list as $rs) {
    $last_id = $rs['id_anuncio'];


		echo '	<a href="anuncio.php?id_anuncio='.$rs['id_anuncio'].'">';

		echo '<div class="panel panel-default">';
		echo '<div class="panel-heading"><h3 style="color:#43a75a">'.$rs['titulo'].'</h3></div>';

		echo '<div class="panel-body">  <img src="'.$rs['imagen_1'].'" alt="'.$rs['id_anuncio'].'"  width="300px"></div>';

		echo ' <div class="panel-body">';
		echo '<h3 align="right" style="color:#43a75a">Precio:'.$rs['precio'].'€</h3>';
		echo ' <div class="panel-footer"><p>'.$rs['categoria'].'<br>Creado el'.$rs['fecha_creacion'].'</p></div>';

		echo '</div>';
		echo '</div>';
		echo '<div>';
		echo '</div>';
		echo '</a>';




}

if ($last_id != 0) {
	echo '<script type="text/javascript">var last_id = '.$last_id.';</script>';
}

//  usamos sleep por 1 segundo para ver el loader
sleep(1);
?>
