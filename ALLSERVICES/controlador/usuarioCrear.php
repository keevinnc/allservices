<?php

  require_once("../modelo/class.conexion.php");
  require_once("../modelo/class.consultas.php");


  #recibimos los datos del form_crearUsuario y
  #saneamos datos:
  $consultas = new consultas();
  $password=$consultas->test_input($_POST['password']);
  //$repite_password=$consultas->test_input($_POST['repite_password']);
  $nombre_usuario=$consultas->test_input($_POST['nombre_usuario']);
  $email=$consultas->test_input($_POST['email']);

  $nombre_usuario_saneado = filter_var($nombre_usuario,FILTER_SANITIZE_STRING);  #Elimina etiquetas, opcionalmente elimina o codifica caracteres especiales
  $password_saneado = filter_var($password,FILTER_SANITIZE_STRING);
  $email_saneado = filter_var($email,FILTER_SANITIZE_EMAIL);  #Elimina todos los caracteres menos letras, dígitos y !#$%&'*+-=?^_`{|}~@.[].
  #validamos los datos:
  if (!filter_var($email_saneado, FILTER_VALIDATE_EMAIL)) {
        header ("Location: ../usuario_crear.php?error_email");
           break;
  }
#comprobamos si el usuario existe.

  $respuesta= $consultas->compruebaUsuario($email_saneado);

 if ($respuesta=="error") {
     header ("Location: ../usuario_crear.php?error_usuario_ya_existe");
        break;
 }
#preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/',$password)
if(!preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/',$password_saneado)) {
       header ("Location: ../usuario_crear.php?error_password");
       break;
  }



  # si todo es correcto, creamos el usuario
    #ciframos password y creamos aplicamos fecha y hora actual
    $password_cifrado=md5($password_saneado);
    $fecha_creacion=date("Y-m-d H:i:s");
  #insertamos datos
    $mensaje=null;
    $mensaje= $consultas->crearUsuario( $email_saneado, $password_cifrado,$nombre_usuario_saneado, $fecha_creacion,2);
  #si se ha creado correctamente redirigimos con ok por GET, para mostrar mensaje

    if($mensaje=="ok"){
        header ("Location: ../usuario_login.php?user_creacion_ok");
         break;
    }else{
        header ("Location: ../usuario_crear.php?error_creacion");
         break;
    }

/*
#algo falla??? no funciona cuando inserto esto, sin esta parte va todo bien

if($mensaje=="ok"){
    header ("Location: ../usuario_login.php?user_creacion_ok");
}else{
    header ("Location: ../usuario_crear.php?error_creacion");
}
*/



 ?>
