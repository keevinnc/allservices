<?php
session_start();

  require_once("../modelo/class.conexion.php");
  require_once("../modelo/class.consultas.php");


#recibimos los datos del form_loginUsuario y
#saneamos datos:
 $consultas = new consultas();
$email=$consultas->test_input($_POST['email']);
$password=$consultas->test_input($_POST['password']);
$password_saneado = filter_var($password,FILTER_SANITIZE_STRING);   #Elimina etiquetas, opcionalmente elimina o codifica caracteres especiales
$email_saneado = filter_var($email,FILTER_SANITIZE_EMAIL);  #Elimina todos los caracteres menos letras, dígitos y !#$%&'*+-=?^_`{|}~@.[].

#validamos los datos:
if (!filter_var($email_saneado, FILTER_VALIDATE_EMAIL)) {
header ("Location: ../usuario_login.php?email_error");
}
if(!preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/',$password_saneado)) {
  /*password no valido.
    - minimo una letra pequeña
    - minimo una letra mayuscula
    - minimo un Numero
    - minimo un caracter especial @#-_$%^&+=§!?
  */
header ("Location: ../usuario_login.php?password_error");
} # si lo datos son validos ejecutamos el login.
else{

  $password_cifrado=md5($password); #ciframos el password

    #consultamos datos

     $respuesta= $consultas->loginUsuario($email_saneado, $password_cifrado);

     //echo $respuesta;


     #dependiendo de la respuesta realizamos la acción oportuna

     if($respuesta != 0){
              $_SESSION["logeado"]="ok";
              foreach ($respuesta as $rs) {

                 $_SESSION["id_usuario"]=$rs['id_usuario'];
                 $_SESSION["email"]=$rs['email'];
                 $_SESSION["nombre"]=$rs['nombre'];
                 $_SESSION["rol"]=$rs['roles_id_rol'];
              }

              if($_SESSION["rol"]=="1"){
                  header ("Location: ../admin/administrar.php");
              }else {
                header ("Location: ../home.php");
              }


       }else{
            header ("Location: ../usuario_login.php?error_login");
             break;
             echo "error";

           }



}












 ?>
