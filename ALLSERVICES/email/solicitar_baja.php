<?php
session_start();
if(!isset($_SESSION['logeado'])){
  header ("Location: ../usuario_login.php?error_contactar_anuncio");
  break;
}

// RECOGEMOS LOS DATOS PARA PROCEDER AL ENVIO DEL EMAIL
$email_solicitante=$_SESSION["email"];




 ?>

<!-- Pagina para darse de baja -->
<html>
  <head>
    <meta charset="utf-8">
    <title>AllServices - tus servicios disponibles las 24H</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="js/jquery-1.12.3.min.js"></script>
    <link rel="stylesheet" href="../css/bootstrap.min.css" >
      <link rel="stylesheet" href="../css/style.css" >

  </head>
  <body>
<!-- inicio header -->
<header>
  <div class="container header_link">
      <h1>Solicitar Baja</h1>
  </div>
</header>
<!-- fin header -->


<!-- inicio  go-home -->

    <nav class="navbar navbar-default navbar-inverse" role="navigation">

      <div class="container">

    			<div class="navbar-header">

    			  <a class="navbar-brand" href="../home.php">AllServices</a>
    			</div>
      </div>
    </nav>

<!-- fin  barra-go-home -->

<!-- inicio container -->
<div class="container col-xs-12  col-sm-6 col-md-6  col-lg-5 col-sm-offset-3 col-md-offset-3 col-lg-offset-4  ">
<br>

<!-- Inicio Formulario crearUsuario -->

<form id="form1"class="form" action="enviar_solicitar_baja.php" method="post" name="form1">

    <div class="well well-sm">

    <div class="form-group">
         <label class="sr-only" for="nombre2">nombre</label>
         <input type="text" class="form-control" id="crear_nombre_usuario" name="Nombre" placeholder="Nombre completo" required>
    </div>

   <div class="form-group">
      <input type="hidden"  id="email" name="email_solicitante"  value="<?=$email_solicitante ?>" >

   </div>


    <div class="form-group">
    <label class="sr-only" for="mensaje2">Motivo</label>
     <textarea id="Mensaje" class="form-control" name="Mensaje" rows="4" placeholder="Solicito la baja por el siguiente motivo..." required></textarea>
   </div>




   <div class="form-group">
      <button type="submit" value="contactar" class="btn btn-success btn-block">Enviar</button><br>


   </div>
</div>
</form>
<!--

<div class="container">

    <form id="form1" class="well col-lg-12" action="enviar.php" method="post" name="form1">
      <div class="row">
       <div class="col-lg-6">
        <label>Nombre*</label> <input id="Nombre" class="form-control" type="text" name="Nombre" required />
        <label>Email*</label> <input id="Email" class="form-control" type="email" name="Email" required />
        <label>Telefono*</label> <input id="Telefono" class="form-control" type="tel" name="Telefono" required />
       </div>
        <div class="col-lg-6"><label>Mensaje*</label>
         <textarea id="Mensaje" class="form-control" name="Mensaje" rows="4" placeholder="Estoy interesado en este servicio.." required></textarea>
        </div>
        <button class="btn btn-default pull-right" type="submit">Enviar</button>
      </div>
    </form>

  </div>
  -->





<!-- fin formulario crearUsuario -->


</div>
<!-- fin container -->



<script  src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
  </body>
</html>
