<?php
session_start();
//var_dump($_SESSION);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>AllServices - tus servicios disponibles las 24H</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/script_scroll.js"></script>
      <link rel="stylesheet" href="css/style.css" >
      <link rel="stylesheet" href="css/bootstrap.min.css" >

  </head>
  <body>


<!-- inicio header -->
        <header>
          <div class="container">
        		<div class="col-md-6">
        			<h1>Home </h1>
        		</div>
          </div>
        </header>
<!-- fin header -->

<!-- inicio  menu-filtro -->

    <nav class="navbar navbar-default navbar-inverse" role="navigation">

      <div class="container">

    			<div class="navbar-header">
    			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
    				<span class="sr-only">barra navegación</span>
    				<span class="icon-bar"></span>
    				<span class="icon-bar"></span>
    				<span class="icon-bar"></span>
    			  </button>
    			  <a class="navbar-brand" href="home.php">AllServices</a>
    			</div>


        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

<!-- inicio buscador -->
        <ul class="nav navbar-nav">
              <li><a href="home.php?puntuaciones"><img  src="images/icons/icon_star.png" alt="icon_star"  width="14px"> Puntuaciones </a></li>
                  <li><a href="tus_anuncios.php">Tus anuncios</a></li>
              <li><a href="anuncio_crear.php">Publicar Anuncio</a></li>

             <form class="navbar-form navbar-left" role="search" method="get">
      				  <div class="form-group">
      				        <input type="text" class="form-control" name="buscar" placeholder="estoy buscando..">
      				  </div>
      				        <button type="submit" class="btn btn-default" value="buscar">Buscar</button>
            </form>
        </ul>
<!-- fin buscador -->

<!-- inicio login -->

        <?php

        if(isset($_SESSION['logeado'])){
        $name_user=$_SESSION["nombre"];
        include 'home_menuLogeado.php';
        }else{
        include 'home_menuSinLogear.php';
        }
        ?>
<!-- fin login -->


        </div>
      </div>
    </nav>


<!-- fin  menu-filtro -->


<!--__________________________________________________________________
________________________CONTENIDO____________________________________
______________________________________________________________________-->



<!-- inicio contenido -->
<div class="container">
  <section class="main row">



    <article class="col-xs-12 col-sm-3 " >




      <!-- Inicio Formulario -->
      <form   method="get">
        <div class="well well-sm">

          <div class="form-group">
                <input type="text" class="form-control" name="buscar" placeholder="estoy buscando..">
          </div>



          <div class="form-group">
            <label class="control-label" for="option">Categoria:</label>
            <?php
            require_once('modelo/class.conexion.php');
            $modelo = new Conexion(); /* creamos un objeto de conexion */
            $pdo = $modelo->get_conexion();  /* aquí guardamos la conexion */

            try {
              $sql = 'SELECT * FROM categorias ';
              $query = $pdo->prepare($sql);
              $query->execute();
              $list = $query->fetchAll();
            }catch (PDOException $e) {
            	echo 'PDOException : '.  $e->getMessage();
            }
            echo '<select class="form-control" name="categoria" id="option"  >';
                echo ' <option  value="">Selecciona </option>';
            foreach ($list as $rs) {
                echo '<option value='.$rs['nombre_categoria'].'>'.$rs['nombre_categoria'].'</option>';
                //<h2>'.$rs['nombre_categoria'].'</h2>';
            }
              echo '</select>';
             ?>
          </div>
<!--
          <div class="form-group">
            <label class="control-label" for="option">Ubicación</label>
            <select class="form-control" name="" id="option">
                <option value="" selected="selected">En toda España</option>
                  <option value="">Barcelona</option>
                  <option value="">Madrid</option>
                  <option value="">Zaragoza</option>
                  <option value="">Valencia</option>
            </select>
          </div>
-->
      <button class="btn btn-success btn-block "  >Aplicar</button>
      </div>
      </form>
      <!-- fin formulario -->









    </article>


    <?php
     if(isset($_GET['buscar'])){
          require_once("modelo/class.conexion.php");
          require_once("modelo/class.consultas.php");
          require_once("controlador/buscar_anuncios.php");

          if(isset($_GET['categoria'])){
          buscar($_GET['buscar'],$_GET['categoria']);
        }else{
          buscar($_GET['buscar'],"");
        }

        }



    else if(isset($_GET["puntuaciones"])){
        include 'puntuaciones.php';
    }else if(isset($_GET["tus_anuncios"])){
          include 'tus_anuncios.php';
      }else {
        include 'listado_anuncios.php';
    }

    ?>

  </section>
</div>


<!-- fin contenido -->




<!-- inicio footer -->
<footer class="col-lg-12">
  <div class="container">
    <br>
    <p>
      Síguenos en las redes sociales:
    </p>
    <p>
      <a href="https://www.facebook.com/"><img  src="images/icons/icon_facebook.png" alt="icon_facebook"  width="40px">  </a>
       <a href="https://twitter.com/?lang=es"><img  src="images/icons/icon_twitter.png" alt="icon_twitter"  width="40px"> </a>
       <a href="https://plus.google.com/about?hl=es"><img  src="images/icons/icon_google_plus.png" alt="icon_google_plus"  width="40px"> </a>
    </p>
  </div>
</footer>
<!-- fin footer -->





<script  src="js/jquery-1.12.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
  </body>
</html>
