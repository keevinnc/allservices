

    <!-- inicio login -->

      <ul class="nav navbar-nav navbar-right">

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Iniciar sesión</b> <span class="caret"></span></a>
			<ul id="login-dp" class="dropdown-menu">
				<li>
					 <div class="row">
							<div class="col-md-12">
    <!-- inicio FORM-login -->
								 <form class="form" role="form" method="post" action="controlador/usuarioLogin.php" accept-charset="UTF-8" id="login-nav">
										<div class="form-group">
											 <label class="sr-only" for="exampleInputEmail2">Email</label>
											 <input type="email" class="form-control" id="exampleInputEmail2" name="email" placeholder="Email" required>
										</div>
										<div class="form-group">
											 <label class="sr-only" for="exampleInputPassword2">Contraseña</label>
											 <input type="password" class="form-control" id="exampleInputPassword2" name="password" placeholder="Contraseña" required>

                       </div>
                      <!--  <div class="help-block text-right"><a href="">olvidaste tu contraseña?</a></div>-->
										</div>
										<div class="form-group">
											 <button type="submit" class="btn btn-success btn-block">Iniciar sesión</button>

											<!-- <a href="#" class="btn btn-fb btn-block"><i class="fa fa-facebook"></i> Facebook</a>-->
										</div>

								 </form>
  <!-- fin FORM-login -->

							</div>
							<div class="bottom text-center">
								¿nuevo usuario? <a href="usuario_crear.php"><b>crear cuenta</b></a>
							</div>
					 </div>
				</li>
			</ul>
        </li>
      </ul>
      <!-- fin login -->
