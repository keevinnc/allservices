<?php

  class Consultas{


      /* ______________________________*/
      /* Funcion para crear un usuario*/
      /* ______________________________*/

            public function crearUsuario( $arg_user_email, $arg_user_password,$arg_user_name, $arg_fecha_creacion,$arg_rol){
              $modelo = new Conexion(); /* creamos un objeto de conexion */
              $conexion = $modelo->get_conexion();  /* aquí guardamos la conexion */
              /* creamos la consulta*/
              $sql = "insert into usuarios (email, password, nombre, fecha_creacion, roles_id_rol) values (:email, :password, :nombre, :fecha_creacion, :roles_id_rol)";
                  /* preparamos la conexion y le pasamos los varoles con BinPAram para evitar el sql injection*/
              $consulta = $conexion ->prepare($sql);
              $consulta->bindParam(":email", $arg_user_email);
              $consulta->bindParam(":password", $arg_user_password);
              $consulta->bindParam(":nombre", $arg_user_name);
              $consulta->bindParam(":fecha_creacion", $arg_fecha_creacion);
              $consulta->bindParam(":roles_id_rol", $arg_rol);

              if(!$consulta){
                return "error";
              }else{
                $consulta->execute();
                return "ok";
              }

            }
            /* ______________________________*/
            /* Funcion comprobar usuario existe*/
            /* ______________________________*/

            public function compruebaUsuario($arg_user_email){
                $modelo = new Conexion(); #creamos la conexion
                $conexion = $modelo->get_conexion();
                $sql= "select * from usuarios where email=:email "; #creamos la consulta
                $consulta = $conexion ->prepare($sql); #preparamos la consulta
                $consulta->bindParam(':email', $arg_user_email, PDO::PARAM_STR); #vinculamos los parametros
                $consulta->execute();
                $total = $consulta->rowCount();
                if($total == 0){
                  return "ok";
                }else{
                  return "error";
                }

            }
            /* __________________________*/
            /* Funcion para sanear datos*/
            /* ________________________*/
              public function test_input($data) {
                $data = trim($data);#con trim, quitamos los espacios en blanco al inicio y final
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
                }

            /* ______________________________*/
            /* Funcion para login de un usuario*/
            /* ______________________________*/

            public function loginUsuario($arg_user_email, $arg_user_password){

                $modelo = new Conexion(); #creamos la conexion
                $conexion = $modelo->get_conexion();
                $sql= "select * from usuarios where email=:email AND password=:password"; #creamos la consulta
                $consulta = $conexion ->prepare($sql); #preparamos la consulta
                $consulta->bindParam(':email', $arg_user_email, PDO::PARAM_STR); #vinculamos los parametros
                $consulta->bindParam(':password', $arg_user_password, PDO::PARAM_STR);
                $consulta->execute();
                $datos_user = $consulta->fetchAll();
                $total = $consulta->rowCount();


                if($total != 0){
                  return $datos_user;
                }


            }

            /* ______________________________*/
            /* Funcion mostrar todos los usuarios*/
            /* ______________________________*/

            public function listaTodosUsuarios(){

              $modelo = new Conexion(); #creamos la conexion
              $conexion = $modelo->get_conexion();
              $sql= "select * from usuarios ";
              $consulta = $conexion ->prepare($sql); #preparamos la consulta
              $consulta->execute();
              $datos_usuarios = $consulta->fetchAll();
              return $datos_usuarios;


            }

            /* ______________________________*/
            /* Funcion para crear Anuncio*/
            /* ______________________________*/

                  public function crearAnuncio($arg_titulo_saneado, $arg_descripcion_saneado, $arg_email, $arg_codigo_postal_saneado, $arg_telefono_saneado, $arg_categoria, $arg_precio, $arg_fecha_creacion, $farg_echa_caduca, $arg_imagen, $usuarios_id_usuario, $categorias_id_categoria){

                    $modelo = new Conexion(); /* creamos un objeto de conexion */
                    $conexion = $modelo->get_conexion();  /* aquí guardamos la conexion */
                    /* creamos la consulta*/
                    $sql = "insert into anuncios (titulo, descripcion, email, codigo_postal, telefono, categoria, precio, fecha_creacion, fecha_caduca, imagen_1 ,usuarios_id_usuario, categorias_id_categoria) values (:titulo, :descripcion,:email, :codigo_postal, :telefono, :categoria, :precio, :fecha_creacion, :fecha_caduca, :imagen, :usuarios_id_usuario, :categorias_id_categoria)";
                        /* preparamos la conexion y le pasamos los varoles con BinPAram para evitar el sql injection*/
                    $consulta = $conexion ->prepare($sql);
                    $consulta->bindParam(":titulo", $arg_titulo_saneado);
                    $consulta->bindParam(":descripcion", $arg_descripcion_saneado);
                    $consulta->bindParam(":email", $arg_email);
                    $consulta->bindParam(":codigo_postal", $arg_codigo_postal_saneado);
                    $consulta->bindParam(":telefono", $arg_telefono_saneado);
                    $consulta->bindParam(":categoria", $arg_categoria);
                    $consulta->bindParam(":precio", $arg_precio);
                    $consulta->bindParam(":fecha_creacion", $arg_fecha_creacion);
                    $consulta->bindParam(":fecha_caduca", $farg_echa_caduca);
                    $consulta->bindParam(":imagen", $arg_imagen);
                    $consulta->bindParam(":usuarios_id_usuario", $usuarios_id_usuario);
                    $consulta->bindParam(":categorias_id_categoria", $categorias_id_categoria);

                    if(!$consulta){
                      return "error";
                    }else{
                      $consulta->execute();
                      return "ok";
                    }

                  }
            /* ______________________________*/
            /* Funcion para crear Anuncio*/
            /* ______________________________*/
                  public function buscarAnuncio($busqueda,$categoria){
                          $filas= null;
                          $modelo = new Conexion();
                          $conexion = $modelo->get_conexion();
                          $title= "%".$busqueda."%";
                          $category= "%".$categoria."%";
                          $sql= "select * from anuncios where titulo like :titulo  AND categoria like :categoria ORDER BY fecha_creacion DESC ";
                          $consulta = $conexion ->prepare($sql);
                          $consulta->bindParam(":titulo", $title);
                          $consulta->bindParam(":categoria", $category);

                          $consulta->execute();
                          while ($resultado= $consulta->fetch()) {
                              $filas[]= $resultado;
                          }
                          return $filas;
                        }

                        /* ______________________________*/
                        /* Funcion mostrar anuncio*/
                        /* ______________________________*/

                        public function consultaAnuncio($arg_id_anuncio){

                          $modelo = new Conexion(); #creamos la conexion
                          $conexion = $modelo->get_conexion();
                          $sql= "select * from anuncios where id_anuncio=:id_anuncio "; #creamos la consulta
                          $consulta = $conexion ->prepare($sql); #preparamos la consulta
                          $consulta->bindParam(':id_anuncio', $arg_id_anuncio, PDO::PARAM_STR); #vinculamos los parametros
                          $consulta->execute();
                          $datos_anuncio = $consulta->fetchAll();
                          return $datos_anuncio;
                        }

                        /* ______________________________*/
                        /* Funcion mostrar tus anuncios*/
                        /* ______________________________*/

                        public function consultaAnuncios($arg_email_usuario){

                          $modelo = new Conexion(); #creamos la conexion
                          $conexion = $modelo->get_conexion();
                          if ($arg_email_usuario=="todos") {
                          $sql= "select * from anuncios ";
                          $consulta = $conexion ->prepare($sql); #preparamos la consulta
                          $consulta->execute();
                          $datos_anuncios = $consulta->fetchAll();
                          return $datos_anuncios;
                          }else {
                              $sql= "select * from anuncios where email=:email "; #creamos la consulta
                            $consulta = $conexion ->prepare($sql); #preparamos la consulta
                            $consulta->bindParam(':email', $arg_email_usuario, PDO::PARAM_STR); #vinculamos los parametros
                            $consulta->execute();
                            $datos_anuncios = $consulta->fetchAll();
                            return $datos_anuncios;
                          }



                        }



                        /* ______________________________*/
                        /* Funcion para borrar anuncio*/
                        /* ______________________________*/

                              public function borrarAnuncio($arg_comparacion,$tipo){
                                $modelo = new Conexion(); /* creamos un objeto de conexion */
                                $conexion = $modelo->get_conexion();  /* aquí guardamos la conexion */
                                /* creamos la consulta*/
                                if ($tipo=="email") {
                                  $sql = "DELETE FROM anuncios WHERE email = :comparacion ";
                                      /* preparamos la conexion y le pasamos los varoles con BinPAram para evitar el sql injection*/
                                  $consulta = $conexion ->prepare($sql);
                                  $consulta->bindParam(":comparacion", $arg_comparacion);

                                  if(!$consulta){
                                   return "error";
                                  }else{
                                    $consulta->execute();
                                    return "ok";
                                  }
                                }else {

                                  $sql = "DELETE FROM anuncios WHERE id_anuncio = :comparacion ";
                                      /* preparamos la conexion y le pasamos los varoles con BinPAram para evitar el sql injection*/
                                  $consulta = $conexion ->prepare($sql);
                                  $consulta->bindParam(":comparacion", $arg_comparacion);

                                  if(!$consulta){
                                   return "error";
                                  }else{
                                    $consulta->execute();
                                    return "ok";
                                  }
                                }


                              }

                              /* ______________________________*/
                              /* Funcion para insertar valoracion*/
                              /* ______________________________*/

                                    public function crearValoracion($puntuacion, $comentario_saneado,$email_usuario_voto, $id_usuario_voto, $id_anuncio,$email_anunciante){

                                      $modelo = new Conexion(); /* creamos un objeto de conexion */
                                      $conexion = $modelo->get_conexion();  /* aquí guardamos la conexion */
                                      /* creamos la consulta*/
                                      $sql = "insert into valoraciones (valoracion, comentario, email_user_voto,email_anunciante, id_user_voto, anuncios_id_anuncio) values (:valoracion, :comentario,:email_user_voto, :email_anunciante, :id_user_voto, :anuncios_id_anuncio)";
                                          /* preparamos la conexion y le pasamos los varoles con BinPAram para evitar el sql injection*/
                                      $consulta = $conexion ->prepare($sql);
                                      $consulta->bindParam(":valoracion", $puntuacion);
                                      $consulta->bindParam(":comentario", $comentario_saneado);
                                      $consulta->bindParam(":email_user_voto", $email_usuario_voto);
                                      $consulta->bindParam(":id_user_voto", $id_usuario_voto);
                                      $consulta->bindParam(":anuncios_id_anuncio", $id_anuncio);
                                      $consulta->bindParam(":email_anunciante", $email_anunciante);
                                      if(!$consulta){
                                        return "error";
                                      }else{
                                        $consulta->execute();
                                        return "ok";
                                      }

                                    }

                                    /* ______________________________*/
                                    /* Funcion mostrar TODAS las Puntuaciones*/
                                    /* ______________________________*/

                                    public function consultaPuntuaciones(){

                                      $modelo = new Conexion(); #creamos la conexion
                                      $conexion = $modelo->get_conexion();
                                      $sql= "select * from valoraciones "; #creamos la consulta
                                      $consulta = $conexion ->prepare($sql); #preparamos la consulta
                                      $consulta->execute();
                                      $datos_puntuaciones = $consulta->fetchAll();
                                      return $datos_puntuaciones;
                                    }

                                    /* ______________________________*/
                                    /* Funcion para borrar una Valoración*/
                                    /* ______________________________*/

                                          public function borrarValoracion($arg_comparacion,$tipo){
                                            $modelo = new Conexion(); /* creamos un objeto de conexion */
                                            $conexion = $modelo->get_conexion();  /* aquí guardamos la conexion */
                                            /* creamos la consulta*/
                                            if ($tipo=="email") {


                                                    $sql = "DELETE FROM valoraciones WHERE email_anunciante = :comparacion ";
                                                        /* preparamos la conexion y le pasamos los varoles con BinPAram para evitar el sql injection*/
                                                    $consulta = $conexion ->prepare($sql);
                                                    $consulta->bindParam(":comparacion", $arg_comparacion);

                                                    if(!$consulta){
                                                     return "error";
                                                    }else{
                                                      $consulta->execute();
                                                      return "ok";
                                                    }

                                            }else {

                                              $sql = "DELETE FROM valoraciones WHERE id_valoracion = :comparacion ";
                                                  /* preparamos la conexion y le pasamos los varoles con BinPAram para evitar el sql injection*/
                                              $consulta = $conexion ->prepare($sql);
                                              $consulta->bindParam(":comparacion", $arg_comparacion);

                                              if(!$consulta){
                                               return "error";
                                              }else{
                                                $consulta->execute();
                                                return "ok";
                                              }

                                            }


                                  }
                                  /* ______________________________*/
                                  /* Funcion para borrar anuncio*/
                                  /* ______________________________*/

                                        public function borrarUsuario($arg_comparacion){
                                          $modelo = new Conexion(); /* creamos un objeto de conexion */
                                          $conexion = $modelo->get_conexion();  /* aquí guardamos la conexion */
                                          /* creamos la consulta*/

                                            $sql = "DELETE FROM usuarios WHERE email = :comparacion ";
                                                /* preparamos la conexion y le pasamos los varoles con BinPAram para evitar el sql injection*/
                                            $consulta = $conexion ->prepare($sql);
                                            $consulta->bindParam(":comparacion", $arg_comparacion);

                                            if(!$consulta){
                                             return "error";
                                            }else{
                                              $consulta->execute();
                                              return "ok";
                                            }



                                        }







  }


 ?>
