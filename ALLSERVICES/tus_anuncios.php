 <!--
 Pagina que lista tus anuncios
 -->
<?php
session_start();
if(!isset($_SESSION['logeado'])){
  header ("Location: usuario_login.php?error_tus_anuncios");
}else{

  require_once('modelo/class.conexion.php');
  require_once('modelo/class.consultas.php');

   $consultas = new consultas();
   $email=$_SESSION['email'];
  $datos_anuncios=$consultas->consultaAnuncios($email);

}

 ?>

<html>
  <head>
    <meta charset="utf-8">
    <title>AllServices - tus servicios disponibles las 24H</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Inicio datatables-->
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>

        <link type="text/css" href="css/jquery.dataTables.min.css" rel="stylesheet"/>
    <!-- fin datatables-->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css" >





  </head>
  <body>

    <script type="text/javascript">
function confirm_delete() {
  return confirm('Esta seguro que quiere borrar este Servicio?');
}

$(document).ready(function() {
  $('#tus_anuncios').DataTable( {
      responsive: true
  } );
} );
</script>
<!-- inicio header -->
<header>
  <div class="container">
      <h1>Tus anuncios</h1>
  </div>
</header>
<!-- fin header -->


<!-- inicio  go-home -->

    <nav class="navbar navbar-default navbar-inverse" role="navigation">

      <div class="container">

    			<div class="navbar-header">

    			  <a class="navbar-brand" href="home.php">AllServices</a>
    			</div>
      </div>
    </nav>

<!-- fin  barra-go-home -->



<!-- inicio container -->
<div class="container col-xs-12  col-sm-12   col-md-12  col-lg-12   ">
<br>
<?php

 ?>

<!-- Inicio PANEL -->
<?php
if(isset($_GET["borrar_ok"])){
  echo "<div id='usuario_creado' class='alert alert-success'>Servicio borrado correctamente</div> ";
}
if(isset($_GET["borrar_error"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Error al borrar el servicio</div> ";
}
if(isset($_GET["borrar_no_permitido"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Usted no puede borrar este anuncio, porque no es el propietario.</div> ";
}
 ?>

<table id="tus_anuncios" class="table table-striped display">
      <thead>
         <tr class="info"><td><h4>Titulo</h4></td><td><h4>Descripcion</h4></td><td><h4>Email</h4></td><td><h4>Codigo postal</h4></td><td><h4>telefono</h4></td><td><h4>Categoria</h4></td><td><h4>Precio</h4></td><td><h4>Fecha creacion</h4></td><td><h4>Ver</h4></td><td><h4>Eliminar</h4></td></tr>
      </thead>
      <tbody>
        <?php
        foreach ($datos_anuncios as $rs) {
              $id_anuncio=$rs['id_anuncio'];
              $titulo=$rs['titulo'];
              $descripcion=$rs['descripcion'];
              $email=$rs['email'];
              $codigo_postal=$rs['codigo_postal'];
              $telefono=$rs['telefono'];
              $categoria=$rs['categoria'];
              $precio=$rs['precio'];
              $fecha_creacion=$rs['fecha_creacion'];


            echo " <tr><td>$titulo</td><td>$descripcion</td><td>$email</td><td>$codigo_postal</td><td>$telefono</td><td>$categoria</td><td>$precio</td><td>$fecha_creacion</td><td><a href='anuncio.php?id_anuncio=$id_anuncio'><span class='glyphicon glyphicon-list-alt'></span> </a></td><td>   <a href='controlador/borrar_anuncio.php?id_anuncio=$id_anuncio&email_anunciante=$email' onclick='return confirm_delete()'><span class='glyphicon glyphicon-remove'></span> </a></td></tr>";
        }
         ?>
      </tbody>
   </table>

<!-- fin PANEL -->


</div>
<!-- fin container -->



  </body>
</html>
