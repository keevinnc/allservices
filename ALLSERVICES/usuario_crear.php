<?php
session_start();
if(isset($_SESSION['logeado'])){
  header ("Location: home.php");
}
 ?>

<!-- Pagina para subir anuncio -->
<html>
  <head>
    <meta charset="utf-8">
    <title>AllServices - tus servicios disponibles las 24H</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" >
      <link rel="stylesheet" href="css/style.css" >

  </head>
  <body>
<!-- inicio header -->
<header>
  <div class="container header_link">
      <h1>Crear cuenta</h1>
  </div>
</header>
<!-- fin header -->


<!-- inicio  go-home -->

    <nav class="navbar navbar-default navbar-inverse" role="navigation">

      <div class="container">

    			<div class="navbar-header">

    			  <a class="navbar-brand" href="home.php">AllServices</a>
    			</div>
      </div>
    </nav>

<!-- fin  barra-go-home -->

<!-- inicio container -->
<div class="container col-xs-12  col-sm-6 col-md-6  col-lg-5 col-sm-offset-3 col-md-offset-3 col-lg-offset-4  ">
<br>
<?php
#ERRORES RECIBIDOS POR GET



if(isset($_GET["error_usuario_ya_existe"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>El usuario ya existe.</div> ";
}

if(isset($_GET["error_password_distintos"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>El password no coindice</div> ";
}

if(isset($_GET["error_password"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>
  La contraseña debe seguir el siguiente patron:<br>
    - minimo una letra pequeña<br>
    - minimo una letra mayuscula<br>
    - minimo un Numero<br>
    - minimo un caracter especial @#-_$%^&+=§!?<br>
    </div> ";
}
if(isset($_GET["error_email"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Error en el formato del email.</div> ";
}

if(isset($_GET["error_creacion"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Error al crear el usuario.</div> ";
}



 ?>
<!-- Inicio Formulario crearUsuario -->


<form class="form" role="form" method="post" action="controlador/usuarioCrear.php" accept-charset="UTF-8" id="crearUsuario">
    <div class="well well-sm">

    <div class="form-group">
           <img class="center-block" src="images/icons/icon_login.png" alt="icon_image"  width="120px"class="img-rounded"><br>
         <label class="sr-only" for="nombre2">nombre</label>
         <input type="text" class="form-control" id="crear_nombre_usuario" name="nombre_usuario" placeholder="Nombre" required>
    </div>

   <div class="form-group">
      <label class="sr-only" for="Email2">Email</label><!--oculta el contenido, solo disponible para "screen readers", ayuda discapacidad -->
      <input type="email" class="form-control" id="email" name="email"  placeholder="Email" required>

   </div>
<!--  <div id="disponible" class="alert alert-success">email disponible</div> -->
<div id="Info" ></div>

   <div class="form-group">
      <label class="sr-only" for="Password2">Contraseña</label>
      <input type="password" id="password" class="form-control"  name="password" placeholder="Contraseña" required>
   </div>
   <div class="form-group">
      <label class="sr-only" for="Password3">Repetir Contraseña</label>
      <input type="password" class="form-control" id="repite_password" name="repite_password" placeholder="Repite Contraseña" required>
   </div>
   <div class="form-group">
      <button type="submit" value="crear usuario" class="btn btn-success btn-block">Crear cuenta</button><br>

    <div class="bottom text-center">
      ¿tienes una cuenta de usuario? <a href="usuario_login.php"><b>iniciar sesión</b></a>
    </div>
   </div>
</div>
</form>
<!-- fin formulario crearUsuario -->
</div>
<!-- fin container -->

  <script type="text/javascript">
  /*inicio:
  comprueba si el password coincide */

  var password = document.getElementById("password")
    , repite_password = document.getElementById("repite_password");

  function validarPassword(){
    if(password.value != repite_password.value) {
      repite_password.setCustomValidity("La contraseña no coincide");
    } else {
      repite_password.setCustomValidity('');
    }
  }

  password.onchange = validarPassword;
  repite_password.onkeyup = validarPassword;

  </script>

<script  src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
  </body>
</html>
