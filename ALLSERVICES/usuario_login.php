<!-- Pagina donde se realiza el login, aparte del home -->
<?php
session_start();
if(isset($_SESSION['logeado'])){
  header ("Location: home.php");
  break;
}
 ?>



<html>
  <head>
    <meta charset="utf-8">
    <title>AllServices - tus servicios disponibles las 24H</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css" >
      <link rel="stylesheet" href="css/style.css" >
  </head>
  <body>
<!-- inicio header -->
<header>
  <div class="container header_link">
      <h1>Iniciar Sesión</h1>
  </div>
</header>
<!-- fin header -->

<!-- inicio  go-home -->

    <nav class="navbar navbar-default navbar-inverse" role="navigation">

      <div class="container">

    			<div class="navbar-header">

    			  <a class="navbar-brand" href="home.php">AllServices</a>
    			</div>
      </div>
    </nav>

<!-- fin  barra-go-home -->



<!-- inicio container -->
<div class="container col-xs-12  col-sm-6 col-md-6  col-lg-5 col-sm-offset-3 col-md-offset-3 col-lg-offset-4  ">
<br>

<?php
if(isset($_GET["error_contactar_anuncio"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Por favor, inicie sesion para contactar con el  anunciante.</div> ";
}
if(isset($_GET["error_valorar_anuncio"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Por favor, inicie sesion para valorar un Anuncio.</div> ";
}
if(isset($_GET["error_tus_anuncios"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Por favor, inicie sesion para acceder a sus anuncios.</div> ";
}
if(isset($_GET["error_publicar_anuncio"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Por favor, inicie sesion para publicar un Anuncio.</div> ";
}
if(isset($_GET["user_creacion_ok"])){
  echo "<div id='usuario_creado' class='alert alert-success'>Usuario creado correctamente, inicie sesión:</div> ";
}if(isset($_GET["error_login"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Usuario o contraseña incorrecto.</div> ";
}
if(isset($_GET["password_error"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Formato de contraseña incorrecto.</div> ";
}
if(isset($_GET["email_error"])){
  echo "<div id='usuario_creado' class='alert alert-danger'>Formato de email incorrecto.</div> ";
}


 ?>


<!-- Inicio Formulario Login -->

<form class="form" role="form" method="post" action="controlador/usuarioLogin.php" accept-charset="UTF-8" id="loginUsuario">
    <div class="well well-sm">
         <div class="form-group">
           <img class="center-block" src="images/icons/icon_login.png" alt="icon_image"  width="120px"class="img-rounded"><br>
            <label class="sr-only" for="email2">Email</label><!--oculta el contenido, solo disponible para "screen readers", ayuda discapacidad -->
          <input type="email" class="form-control" id="email" name="email"  placeholder="Email" required>
         </div>
         <div class="form-group">
            <label class="sr-only" for="password2">Contraseña</label>
                  <input type="password" id="password" class="form-control"  name="password" placeholder="Contraseña" required>

            <div class="help-block text-right"><a href="">olvidaste tu contraseña?</a></div>
         </div>
   <div class="form-group">
      <button type="submit" class="btn btn-success btn-block">Iniciar sesión</button><br>
<!-- Proximamente:
      <p class="text-center">login con:</p>
     <a href="#" class="btn btn-fb btn-block"><i class="fa fa-facebook"></i> Facebook</a>
   -->
    <p class="text-center"></p>
    <div class="bottom text-center">
      ¿nuevo usuario? <a href="usuario_crear.php"><b>crear cuenta</b></a>
    </div>
   </div>
</div>
</form>
<!-- fin formulario Login -->
</div>
<!-- fin container -->





<script  src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
  </body>
</html>
