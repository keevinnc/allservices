# AllServices ️📦



El proyecto se basa en una web donde los usuarios pueden subir anuncios de servicios (ejemplo cerrajero, albañil..),
donde se mostrará la franja horaria en la que se puede realizar y un mapa con la ubicación.

Además los usuarios pueden contactar a traves del formulario del anunció, haciendo llegar un correo al usuario del anuncio.

En la parte principal tenemos un buscador global de anuncios y también filtros para hacer más sencilla la busqueda.
Para evitar la sobrecarga, en la vista principal los anuncios se precargan y cuando llegamos al tope, hacemos una nueva llamada los demás anuncios usando el metodo infinte scroll.

Todas las páginas tienen un diseño responsive usanbo bootstrap.

## Datos destacar:

* **Login en la pantalla principal y además pantalla invidual.** 
* **Registros de usuarios y anuncios** 
* **Diseño responsive (bootstrap)** 
* **Infinite Scroll con JQUERY/AJAX** 
* **Buscador global de anuncios** 
* **Datatables para administración de usuarios, anuncios...** 
* **CRUD completo para usuarios y anuncios** 
* **Etc...** 

    

La ultima amplación que se intentó realizar fué un chat para usuarios(en proceso).

Proyecto de final de curso. Nota final: 8.

## Autor ✒️


* **Kevin Chong B.** - *Desarollador Web Full Stack* - [keevinnc](https://gitlab.com/keevinnc)

## Página principal 

![Imagen 1](https://gitlab.com/keevinnc/allservices/raw/master/proyecto_final.png)