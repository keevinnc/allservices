-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 11-06-2016 a las 19:45:52
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `allservices`
--
CREATE DATABASE IF NOT EXISTS `allservices` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `allservices`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anuncios`
--

CREATE TABLE `anuncios` (
  `id_anuncio` int(11) NOT NULL,
  `titulo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `descripcion` varchar(300) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `codigo_postal` int(5) UNSIGNED ZEROFILL NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `categoria` varchar(45) CHARACTER SET utf8 NOT NULL,
  `precio` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_caduca` datetime NOT NULL,
  `imagen_1` varchar(100) CHARACTER SET utf8 NOT NULL,
  `imagen_2` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `imagen_3` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `usuarios_id_usuario` int(11) NOT NULL,
  `categorias_id_categoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `anuncios`
--

INSERT INTO `anuncios` (`id_anuncio`, `titulo`, `descripcion`, `email`, `codigo_postal`, `telefono`, `categoria`, `precio`, `fecha_creacion`, `fecha_caduca`, `imagen_1`, `imagen_2`, `imagen_3`, `usuarios_id_usuario`, `categorias_id_categoria`) VALUES
(47, 'Buceo en lloret', 'Se ofrecen clases de buceo, muchos años de experiencia y material profesional disponible.', 'alberto@gmail.com', 08001, 628423928, 'Ocio', 20, '2016-06-11 03:14:08', '2016-07-11 03:14:08', 'images/img_anuncios/buceo.jpg', NULL, NULL, 14, 1),
(48, 'Chofer de limusina', 'Quieres que te lleven a cualquier lugar?, si quieres puedes disfrutar de una estupenda limusina. llamame o contacta conm', 'alberto@gmail.com', 08008, 628423928, 'Servicios', 30, '2016-06-11 03:16:33', '2016-07-11 03:16:33', 'images/img_anuncios/chofer.jpg', NULL, NULL, 14, 1),
(49, 'Entrenador personal', 'Ponte a forma este verano, consigue el cuerpo que siempre has deseado.', 'alberto@gmail.com', 08042, 628423928, 'Deporte', 12, '2016-06-11 03:17:48', '2016-07-11 03:17:48', 'images/img_anuncios/entrenador-personal1.jpg', NULL, NULL, 14, 1),
(50, 'Masajista a domicilio', 'Tienes estres? sufres de dolores.. date un capricho y te sentiras mejor, llamame.\r\nSoy una estupenda masajista. solo per', 'kchong@iesjoandaustria.org', 08912, 628423928, 'Cuidado', 23, '2016-06-11 03:21:01', '2016-07-11 03:21:01', 'images/img_anuncios/masajista.jpg', NULL, NULL, 2, 1),
(51, 'Enfermera privada', 'Si necesitas cuidados especiales de una profesional, ponte en contacto conmigo.', 'kchong@iesjoandaustria.org', 08021, 636917642, 'Enfermeria', 30, '2016-06-11 03:22:33', '2016-07-11 03:22:33', 'images/img_anuncios/enfermera.jpg', NULL, NULL, 2, 1),
(52, 'limpieza de cristales profesional', 'Se ofrece limpiador de cristales, mamparas, ventanales y parecidos.\r\ncuento con muchos aÃ±os de experienca en el sector.', 'kchong@iesjoandaustria.org', 08912, 628423928, 'Limpieza', 8, '2016-06-11 03:24:30', '2016-07-11 03:24:30', 'images/img_anuncios/limpieza-cristales.jpg', NULL, NULL, 2, 1),
(53, 'Risoterapia', 'lo mejor para desconectar es poder divertirse, trabajamos en grupos, con la risoterapia podras unir lazos en el trabajo,', 'kchong@iesjoandaustria.org', 08040, 628423928, 'Ocio', 12, '2016-06-11 03:27:03', '2016-07-11 03:27:03', 'images/img_anuncios/risoterapia.jpg', NULL, NULL, 2, 1),
(54, 'Cultivador de patatas', 'Se ofrece cultivador experto con muchos aÃ±os en el sector, sobre todo en el cultivo de patatas.', 'kchong@iesjoandaustria.org', 08911, 628423928, 'Jardineria', 13, '2016-06-11 03:28:24', '2016-07-11 03:28:24', 'images/img_anuncios/agricultor.jpg', NULL, NULL, 2, 1),
(55, 'Paseadora de perros', 'Paseo perros de todas las razas por Badalona a cualquier hora del dÃ­a.', 'erika.j.r93@gmail.com', 08913, 678323053, 'Servicios', 8, '2016-06-11 11:45:32', '2016-07-11 11:45:32', 'images/img_anuncios/Nombres-para-perros-485x300.jpg', NULL, NULL, 15, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(45) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `nombre_categoria`, `descripcion`) VALUES
(1, 'Fontaneria', 'fontaneros en accion'),
(2, 'Informatica', 'programador, servicio tecnico de ordenadores'),
(3, 'Enfermeria', 'Enfermeras, cuidadores especializados..'),
(5, 'Limpieza', 'servicios de limpieza general'),
(6, 'Jardineria', 'cuidado de jardines'),
(7, 'Contruccion', 'todo lo relacionado con la construccion'),
(8, 'Cuidado personal', 'belleza, maquilladoras, masajistas..'),
(9, 'Mecanica', 'mecanicos de coches, motos..'),
(10, 'Ocio', 'actividades de ocio'),
(11, 'Servicios privados', 'chofer..'),
(12, 'Deporte', 'entrenadores personales'),
(13, 'Electricidad', 'electricistas, tecnicos de redes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `imagen_id` int(11) NOT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `nombre`) VALUES
(1, 'administrador'),
(2, 'usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `email` varchar(45) CHARACTER SET utf8 NOT NULL,
  `password` varchar(45) CHARACTER SET utf8 NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `roles_id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `email`, `password`, `nombre`, `fecha_creacion`, `roles_id_rol`) VALUES
(2, 'kchong@iesjoandaustria.org', '018854590872a82e80d3813ecf7d4248', 'erika julian', '2016-06-08 23:00:09', 2),
(14, 'alberto@gmail.com', '018854590872a82e80d3813ecf7d4248', 'Alberto', '2016-06-11 02:58:30', 2),
(15, 'erika.j.r93@gmail.com', '48bd361bdac2b0316563bcb5dcad700e', 'erika', '2016-06-11 11:38:59', 2),
(16, 'keevinnc92@gmail.com', '3f38f20144c070bd8bf89b876100e8e1', 'Administrador', '2016-06-11 14:10:25', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valoraciones`
--

CREATE TABLE `valoraciones` (
  `id_valoracion` int(11) NOT NULL,
  `valoracion` int(11) NOT NULL,
  `comentario` varchar(45) NOT NULL,
  `email_user_voto` varchar(50) NOT NULL,
  `email_anunciante` varchar(45) NOT NULL,
  `id_user_voto` varchar(45) DEFAULT NULL,
  `anuncios_id_anuncio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `valoraciones`
--

INSERT INTO `valoraciones` (`id_valoracion`, `valoracion`, `comentario`, `email_user_voto`, `email_anunciante`, `id_user_voto`, `anuncios_id_anuncio`) VALUES
(27, 4, 'muy buen servicio', 'kchong@iesjoandaustria.org', 'alberto@gmail.com', '2', 48),
(29, 4, 'Consegui perder esos fastidiosos kg que me so', 'kchong@iesjoandaustria.org', 'alberto@gmail.com', '2', 49),
(30, 5, 'nos reimos mucho, gracias por la atencion', 'alberto@gmail.com', 'kchong@iesjoandaustria.org', '14', 53),
(31, 1, 'muy mal, la patatas estaban pochas.', 'alberto@gmail.com', 'kchong@iesjoandaustria.org', '14', 54),
(32, 4, 'Nos divertimos mucho', 'keevinnc92@gmail.com', 'alberto@gmail.com', '1', 47),
(33, 1, 'muy mal, no me gusto nada el masaje.', 'keevinnc92@gmail.com', 'kchong@iesjoandaustria.org', '1', 50),
(34, 5, 'El perro esta muy contento con ella.', 'erika.j.r93@gmail.com', 'erika.j.r93@gmail.com', '15', 55),
(35, 5, 'todo correcto', 'kchong@iesjoandaustria.org', 'alberto@gmail.com', '2', 47);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `anuncios`
--
ALTER TABLE `anuncios`
  ADD PRIMARY KEY (`id_anuncio`,`usuarios_id_usuario`,`categorias_id_categoria`),
  ADD KEY `fk_anuncios_usuarios_idx` (`usuarios_id_usuario`),
  ADD KEY `fk_anuncios_categorias1_idx` (`categorias_id_categoria`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`imagen_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`,`roles_id_rol`),
  ADD KEY `fk_usuarios_roles1_idx` (`roles_id_rol`);

--
-- Indices de la tabla `valoraciones`
--
ALTER TABLE `valoraciones`
  ADD PRIMARY KEY (`id_valoracion`),
  ADD KEY `fk_valoraciones_anuncios1_idx` (`anuncios_id_anuncio`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `anuncios`
--
ALTER TABLE `anuncios`
  MODIFY `id_anuncio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `imagen_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `valoraciones`
--
ALTER TABLE `valoraciones`
  MODIFY `id_valoracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `anuncios`
--
ALTER TABLE `anuncios`
  ADD CONSTRAINT `fk_anuncios_categorias1` FOREIGN KEY (`categorias_id_categoria`) REFERENCES `categorias` (`id_categoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_anuncios_usuarios` FOREIGN KEY (`usuarios_id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_roles1` FOREIGN KEY (`roles_id_rol`) REFERENCES `roles` (`id_rol`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `valoraciones`
--
ALTER TABLE `valoraciones`
  ADD CONSTRAINT `fk_valoraciones_anuncios1` FOREIGN KEY (`anuncios_id_anuncio`) REFERENCES `anuncios` (`id_anuncio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
